# algorithms-solutions

[Counting Anagrams](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/CountingAnagrams.java)

[Letter Count](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/LetterCount.java)

[String Expression](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/StringExpression.java)

[Swap Case](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/SwapCase.java)

[Rectangles Strip](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/RectanglesStrip.java)

[MaxPathFromTheLeftTopCorner](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/MaxPathFromTheLeftTopCorner.java)

[Odd Network](https://gitlab.com/pabasarajw/algorithms-solutions/-/blob/main/src/main/java/com/algorithms/solutions/OddNetwork.java)
