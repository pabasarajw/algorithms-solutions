package com.algorithms.solutions;

import java.util.*;

public class OddNetwork {
    int count = 0;
    public int solution(int[] A, int[] B) {
        Map<Integer, List<Integer>> map = new HashMap<>();

        for(int i = 0; i < A.length; i++){
            List<Integer> la = map.get(A[i]);
            if(la == null) {
                la = new ArrayList<>();
            }
            la.add(B[i]);
            map.put(A[i], la);

            List<Integer> lb = map.get(B[i]);
            if(lb == null) {
                lb = new ArrayList<>();
            }
            lb.add(A[i]);
            map.put(B[i], lb);
        }

        List<String> lst = new ArrayList<>();

        for(int i = 0; i < map.size(); i++){
            countLinks(i, -1, map, 0, lst, String.valueOf(i));
        }

        return count + A.length;
    }

    public void countLinks(int node, int prevNode, Map<Integer, List<Integer>> map, int c, List<String> lst, String s){
        c += 1;
        System.out.println(s);
        if(map.get(node).size() == 1 && map.get(node).get(0) == prevNode){
            System.out.println("break " + c);

            if((c - 1) != 1 && (c - 1) % 2 != 0) {
                StringBuilder sb = new StringBuilder(s);
                sb.reverse();
                System.out.println("s : " + s + " ss : " + sb.toString());
                if(!lst.stream().anyMatch(ss -> ss.equals(s) || ss.equals(sb.toString()))){
                    count++;
                    lst.add(s);
                    System.out.println("count " + count);
                }
            }

        } else{

            List<Integer> ll = map.get(node);

            if((c - 1) != 1 && (c - 1) % 2 != 0) {
                StringBuilder sb = new StringBuilder(s);
                sb.reverse();
                System.out.println("s : " + s + " ss : " + sb.toString());
                if(!lst.stream().anyMatch(ss -> ss.equals(s) || ss.equals(sb.toString()))){
                    count++;
                    lst.add(s);
                    System.out.println("count " + count);
                }
            }

            for(int i = 0; i < ll.size(); i++){

                if(ll.get(i) != prevNode) {
                    String ss = s + String.valueOf(ll.get(i));
                    countLinks(ll.get(i), node, map, c, lst, ss);

                }
            }
        }
    }
}
