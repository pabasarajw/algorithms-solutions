/*
Question:
Have the function StringExpression(str) read the str parameter being passed which will contain the written out version
of the numbers 0-9 and the words "minus" or "plus" and convert the expression into an actual final number written out as well.
For example: if str is "foursixminustwotwoplusonezero" then this converts to "46 - 22 + 10" which evaluates to 34 and your program
should return the final string threefour. If your final answer is negative it should include the word "negative."
*/

package com.algorithms.solutions;

import java.util.*;
import java.util.stream.Collectors;

public class StringExpression {

    public static void main (String[] args) {
        System.out.println(StringExpression("foursixminustwotwoplusonezero"));
    }

    public static String StringExpression(String str) {
        Map<String, Object> mapNo =  new HashMap<>();
        mapNo.put("zero", 0);
        mapNo.put("one", 1);
        mapNo.put("two", 2);
        mapNo.put("three", 3);
        mapNo.put("four", 4);
        mapNo.put("five", 5);
        mapNo.put("six", 6);
        mapNo.put("seven", 7);
        mapNo.put("eight", 8);
        mapNo.put("nine", 9);
        mapNo.put("plus", "+");
        mapNo.put("minus", "-");

        List<String> list = new ArrayList<>();

        String ss = "";
        String no = "";

        for(int i = 0; i < str.length(); i++){

            ss += str.charAt(i);

            if(mapNo.get(ss) != null){

                Object value = mapNo.get(ss);

                if(value instanceof Integer){
                    no+=value.toString();
                }else{
                    list.add(no);
                    list.add(value.toString());
                    no = "";
                }

                ss = "";
            }
        }
        list.add(no);

        int index = 0;
        int total = Integer.valueOf(list.get(index));

        while(index < list.size()){
            if(list.get(index).equals("+")){
                total += Integer.valueOf(list.get(index + 1));
                index++;
            }else if(list.get(index).equals("-")){
                total -= Integer.valueOf(list.get(index + 1));
                index++;
            }
            index++;
        }

        String finalVal = String.valueOf(total).chars()
                .mapToObj(c -> (char) c)
                .flatMap(f -> mapNo.entrySet().stream().filter(e -> e.getValue().toString().charAt(0)  == f ))
                .map(e -> e.getKey())
                .filter(x -> !x.equals("minus"))
                .collect(Collectors.joining());

        finalVal = total > 10 ? finalVal : "negative" + finalVal;

        return finalVal;
    }

}
