/*
Question:
Have the function LetterCount(str) take the str parameter being passed and return the first word with the greatest number of repeated letters.
For example: "Today, is the greatest day ever!" should return greatest because it has 2 e's (and 2 t's) and it comes before ever which also has 2 e's.
If there are no words with repeating letters return -1. Words will be separated by spaces.
*/

package com.algorithms.solutions;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LetterCount {

    public static void main (String[] args) {
        System.out.print(LetterCount("Today, is the greatest day ever!"));
    }

    public static String LetterCount(String str) {
        String[] splited = str.split(" ");

        String selected = "";
        Long maxCount = 0L;

        for(String s : splited){

            Map<Character, Long> map = s.chars()
                    .mapToObj(c -> (char) c)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            Long count = map.entrySet().stream()
                    .map(e -> e.getValue())
                    .filter(v -> v > 1)
                    .max(Comparator.comparingLong(x -> x)).orElse(-1L);

            if(maxCount < count){
                maxCount = count;
                selected = s;
            }

        }

        return selected.isBlank() ? "-1" : selected;
    }
}
