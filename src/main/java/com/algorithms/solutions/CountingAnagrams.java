/*
Question :
Have the function CountingAnagrams(str) take the str parameter and determine how many anagrams exist in the string.
An anagram is a new word that is produced from rearranging the characters in a different word, for example: cars and arcs are anagrams.
Your program should determine how many anagrams exist in a given string and return the total number.
For example: if str is "aa aa odg dog gdo" then your program should return 2 because "dog" and "gdo" are anagrams of "odg".
The word "aa" occurs twice in the string but it isn't an anagram because it is the same word just repeated.
The string will contain only spaces and lowercase letters, no punctuation, numbers, or uppercase letters.
 */

package com.algorithms.solutions;

import java.util.*;
import java.util.stream.Collectors;

public class CountingAnagrams {

    public static void main (String[] args) {
        System.out.println(CountingAnagrams("aa aa odg dog gdo"));
    }

    public static String CountingAnagrams(String str) {
        Set<String> set = Arrays.stream(str.split(" ")).collect(Collectors.toSet());

        List<Map<Character, Long>> list = new ArrayList<>();

        for(String s : set){
            Map<Character, Long> map = s.chars().mapToObj(c -> (char) c).collect(Collectors.groupingBy(c -> c, Collectors.counting()));
            list.add(map);
        }

        Map<Map<Character, Long>, Long> map = list.stream().collect(Collectors.groupingBy(x -> x, Collectors.counting()));
        Long total = map.entrySet().stream()
                .map(x -> x.getValue())
                .reduce(0L, (a, b) -> a + b);

        return String.valueOf(total - map.size());
    }
}
