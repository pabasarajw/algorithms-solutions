package com.algorithms.solutions;

public class MaxPathFromTheLeftTopCorner {
    String max = "";
    public String solution(int[][] A) {
        findPath(A, 0, 0, "");
        return max;

    }

    public void findPath(int[][] A, int r, int c, String str){
        int rows = A.length;
        int col = A[r].length;

        str = str + A[r][c];

        if(rows - 1 <= r && col - 1 <= c){
            max = max.compareTo(str) < 0 ? str : max;
        }

        if(c+1 < col){
            findPath(A, r, c + 1, str);
        }
        if(r+1 < rows){
            findPath(A, r + 1, c, str);
        }
    }
}
