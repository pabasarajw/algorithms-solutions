package com.algorithms.solutions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlgorithmsSolutionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AlgorithmsSolutionsApplication.class, args);
	}

}
