package com.algorithms.solutions;

import java.util.*;

public class RectanglesStrip {
    public int solution(int[] A, int[] B) {
        Map<Integer,Integer> cMap = new HashMap<>();

        for(int i = 0; i < A.length; i++){

            if(cMap.get(A[i]) == null)  cMap.put(A[i], 1);
            else cMap.put(A[i], cMap.get(A[i]) + 1);

            if(A[i] != B[i]){
                if(cMap.get(B[i]) == null)  cMap.put(B[i], 1);
                else cMap.put(B[i], cMap.get(B[i]) + 1);
            }
        }

        return cMap.entrySet().stream().map(e -> e.getValue()).max(Integer::compareTo).orElse(0);
    }
}
