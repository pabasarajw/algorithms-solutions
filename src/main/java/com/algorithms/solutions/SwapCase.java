
/*
Question:
Have the function SwapCase(str) take the str parameter and swap the case of each character.
For example: if str is "Hello World" the output should be hELLO wORLD. Let numbers and symbols stay the way they are.
*/
package com.algorithms.solutions;

public class SwapCase {

    public static void main (String[] args) {
        System.out.print(SwapCase("Hello World"));
    }

    public static String SwapCase(String str) {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < str.length(); i++){
            char c = str.charAt(i);
            if(Character.isLetter(c) && Character.isLowerCase(c)){
                sb.append(Character.toUpperCase(c));
            } else if(Character.isLetter(c) && Character.isUpperCase(c)){
                sb.append(Character.toLowerCase(c));
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
